# ☃️ ❄️ Advent of Code 2019 ❄️ ☃️

This repository contains solutions implemented in [Go](https://play.golang.org/) for the  [**Advent of Code 2019**](https://adventofcode.com/2019) puzzles.

Each program requires an `input.txt` and outputs the results of each part on a separate line.


### [**Day 7**](7/main.go) &nbsp; 🎛️ ([Amplification Circuit](https://adventofcode.com/2019/day/7))

### [**Day 6**](6/main.go) &nbsp; 🌒 ([Universal Orbit Map](https://adventofcode.com/2019/day/6))

### [**Day 5**](5/main.go) &nbsp; ☄️ ([Sunny with a Chance of Asteroids](https://adventofcode.com/2019/day/5))

### [**Day 4**](4/main.go) &nbsp; 🔐 ([Secure Container](https://adventofcode.com/2019/day/4))

### [**Day 3**](3/main.go) &nbsp; 📐 ([Crossed Wires](https://adventofcode.com/2019/day/3))

### [**Day 2**](2/main.go) &nbsp; 🔥 ([1202 Program Alarm](https://adventofcode.com/2019/day/2))

### [**Day 1**](1/main.go) &nbsp; 🚀 ([The Tyranny of the Rocket Equation](https://adventofcode.com/2019/day/1))
