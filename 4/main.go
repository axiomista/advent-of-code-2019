package main

import (
	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
)

// Day 4: https://adventofcode.com/2019/day/4
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	fmt.Println("Part 1: " + PartOne(string(bytes)))
	fmt.Println("Part 2: " + PartTwo())
}

// PossiblePasswords is all possible passwords given initial criteria (before part 2)
var PossiblePasswords []int

// PartOne returns a count:
// How many different passwords within the range given in input meet these criteria?
func PartOne(input string) string {

	// Password is a six-digit number.
	min := 100000
	max := 999999
	// The value is within the range given in puzzle input.
	r := strings.Split(input, "-")
	left, _ := strconv.Atoi(r[0]) 
	right, _ := strconv.Atoi(r[1]) 
	if left > min {
		min = left
	}
	if right < max {
		max = right
	}

	for password := min; password <= max; password++ {
		passwordStr := strconv.Itoa(password)
		// Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
		allIncreasing := true
		for i :=0; i < 5; i++ {
			if passwordStr[i] > passwordStr[i+1] {
				allIncreasing = false
				break
			}
		}
		// Two adjacent digits are the same (like 22 in 122345).
		adjacentSame := false
		for i :=0; i < 5; i++ {
			if passwordStr[i] == passwordStr[i+1] {
				adjacentSame = true
			}
		}
		if adjacentSame && allIncreasing {
			PossiblePasswords = append(PossiblePasswords, password)
		}
	}

	return strconv.Itoa(len(PossiblePasswords))
}

// PartTwo returns the above but with an extra rule:
// No two adjacent matching digits are not part of a larger group of matching digits
// e.g. 123444 is no good because there are 3 fours, not 2
func PartTwo() string {
	var restrictedPossiblePasswords []int

	// Look at PossiblePasswords and remove any with >2 same digits in a row (e.g. 123444)
	// Important: if there are more than two in a row but exactly 2 elsewhere, it's good (e.g. 111122)
	for _, password := range PossiblePasswords {
		passwordStr := strconv.Itoa(password)
		onlyTwo := false
		if passwordStr[0] == passwordStr[1] && passwordStr[1] != passwordStr[2] {
			onlyTwo = true
		} else if passwordStr[3] != passwordStr[4] && passwordStr[4] == passwordStr[5] {
			onlyTwo = true
		} else {
			for i := 0; i < 3; i++ {
				if passwordStr[i] != passwordStr[i+1] && passwordStr[i+1] == passwordStr[i+2] && passwordStr[i+2] != passwordStr[i+3] {
					onlyTwo = true
				}
			}
		}
		if onlyTwo {
			restrictedPossiblePasswords = append(restrictedPossiblePasswords, password)
		}
	}
	return strconv.Itoa(len(restrictedPossiblePasswords))
}
