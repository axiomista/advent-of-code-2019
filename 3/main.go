package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

// Day 3: https://adventofcode.com/2019/day/3
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	CreateWiresFindIntersections(string(bytes))
	// What is the Manhattan distance from the central port to the closest intersection?
	fmt.Println("Part 1: " + PartOne())
	// What is the fewest combined steps the wires must take to reach an intersection?
	fmt.Println("Part 2: " + PartTwo())
}

// Point is an (x,y) coordinate offset from the "central port" (0,0)
type Point struct {
	x, y int
}

// Wire is a collection of points on a control panel grid beginning at (0,0)
type Wire struct {
	points []Point
}

var WireOne Wire
var WireTwo Wire
var Intersections []Point

// CreateWiresFindIntersections takes input representing two wires and their traversal over a grid,
// setting up structs for WireOne and WireTwo and the set of intersecting points Intersections.
func CreateWiresFindIntersections(input string) {
	for i, s := range strings.Split(input, "\n") {
		switch i {
		case 0:
			WireOne = MakeWire(s)
		case 1:
			WireTwo = MakeWire(s)
		default:
			break
		}
	}
	// Find the intersections of the two wires, not including the origin "central port" at (0,0)
	for _, p1 := range WireOne.points {
		if p1.x == 0 && p1.y == 0 {
			continue
		}
		for _, p2 := range WireTwo.points {
			if p1 == p2 {
				Intersections = append(Intersections, p1)
			}
		}
	}
}

// PartOne returns the smallest number of "blocks" between any intersection and the "central port".
func PartOne() string {
	// Find the *first* distance between an intersection (x, y) and the "central port"
	var minDistance int
	first := Intersections[0]
	minDistance = int(math.Abs(float64(first.x)) + math.Abs(float64(first.y)))
	// Now find the *minimum* distance
	for _, p := range Intersections {
		distance := math.Abs(float64(p.x)) + math.Abs(float64(p.y))
		if int(distance) < minDistance && int(distance) != 0 {
			minDistance = int(distance)
		}
	}
	// Return distance as a string value
	return strconv.Itoa(minDistance)
}

// PartTwo calculates the number of steps each wire takes to reach each intersection.
// It returns the the number of steps for the intersection where the sum of both wires' steps is lowest.
func PartTwo() string {
	// Find the *first* number of wire steps to an intersection (x, y) from the "central port"
	var minSteps int
	var wireOneSteps int
	var wireTwoSteps int

	first := Intersections[0]
	for i, wp := range WireOne.points {
		if wp.x == first.x && wp.y == first.y {
			// wireOneSteps is the position i of (first.x, first.y) in wireOne.points
			wireOneSteps = i
			break
		}
	}
	for i, wp := range WireTwo.points {
		if wp.x == first.x && wp.y == first.y {
			// wireTwoSteps int is the position i of (first.x, first.y) in wireTwo.points
			wireTwoSteps = i
			break
		}
	}
	minSteps = wireOneSteps + wireTwoSteps
	// Now find the *minimum* steps through both wires to an intersection
	for _, p := range Intersections {
		for i, wp := range WireOne.points {
			if wp.x == p.x && wp.y == p.y {
				// wireOneSteps int is the position i of (p.x, p.y) in wireOne.points
				wireOneSteps = i
				break
			}
		}
		for i, wp := range WireTwo.points {
			if wp.x == p.x && wp.y == p.y {
				// wireTwoSteps int is the position i of (p.x, p.y) in wireTwo.points
				wireTwoSteps = i
				break
			}
		}
		steps := wireOneSteps + wireTwoSteps
		if steps < minSteps && steps != 0 {
			minSteps = steps
		}
	}
	return strconv.Itoa(minSteps)
}

// MakeWire builds a Wire struct using the instructions in w that contains all points in path relative to (0,0)
func MakeWire(w string) Wire {
	var newWire Wire
	origin := Point{0, 0}
	// Begin at point (0,0)
	newWire.points = append(newWire.points, origin)
	// Move L (-n,0), R (n,0), U (0,n), or D (0,-n) by n units per comma-delimited instructions
	for _, s := range strings.Split(w, ",") {
		wireLength := len(newWire.points)
		lastPt := newWire.points[wireLength-1]
		direction := s[:1]
		n, _ := strconv.Atoi(s[1:])
		switch direction {
		case "L":
			// From lastPt, go left n times
			for i := 1; i <= n; i++ {
				newPt := Point{lastPt.x - i, lastPt.y}
				newWire.points = append(newWire.points, newPt)
			}
		case "R":
			// From lastPt, go right n times
			for i := 1; i <= n; i++ {
				newPt := Point{lastPt.x + i, lastPt.y}
				newWire.points = append(newWire.points, newPt)
			}
		case "U":
			// From lastPt, go up n times
			for i := 1; i <= n; i++ {
				newPt := Point{lastPt.x, lastPt.y + i}
				newWire.points = append(newWire.points, newPt)
			}
		case "D":
			// From lastPt, go down n times
			for i := 1; i <= n; i++ {
				newPt := Point{lastPt.x, lastPt.y - i}
				newWire.points = append(newWire.points, newPt)
			}
		default:
			fmt.Println("Malformed direction in input string")
			break
		}
	}
	return newWire
}
