package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

// Day 1: https://adventofcode.com/2019/day/1
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	// What is the sum of the fuel requirements for all of the modules on your spacecraft?
	fmt.Println("Part 1: " + PartOne(string(bytes)))
	fmt.Println("Part 2: " + PartTwo(string(bytes)))
}

// PartOne takes the input (each line represents the mass of a module) and calculates the fuel need,
// then adds together all the fuel values.
func PartOne(input string) string {
	var totalFuel int

	for _, s := range strings.Split(input, "\n") {
		moduleMass, _ := strconv.Atoi(s)
		totalFuel += moduleFuel(moduleMass)
	}
	return strconv.Itoa(totalFuel)
}

// PartTwo is different: take each module mass, calculate its fuel and add it to the total;
// then, treat the fuel amount you just calculated as the input mass and repeat, continuing until a fuel requirement is zero or negative.
func PartTwo(input string) string {
	var totalFuel int

	for _, s := range strings.Split(input, "\n") {
		moduleMass, _ := strconv.Atoi(s)
		// Calculate moduleFuel(inputMass) for each new fuel amount and add it to the total
		totalFuel += fuelForFuel(moduleFuel(moduleMass))
	}
	return strconv.Itoa(totalFuel)
}

// moduleFuel is the fuel required to launch a given module is based on its mass:
// Take its mass, divide by 3, round down, and subtract 2
func moduleFuel(mass int) int {
	fuel := int(math.Floor(float64(mass)/3)) - 2
	return fuel
}

// fuelForFuel recursively determines all the fuel needed for the existing fuel
func fuelForFuel(mass int) int {
	if mass <= 0 {
		return 0
	}
	return mass + fuelForFuel(moduleFuel(mass))
}
