package main

import (
	"flag"
	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
)

// Day 5: https://adventofcode.com/2019/day/5

// Pass input as `go run main.go -input1 1 -input2 5`
var inputOne, inputTwo int

func init() {
	flag.IntVar(&inputOne, "input1", 1, "Input for Part One")
	flag.IntVar(&inputTwo, "input2", 5, "Input for Part Two")
}

func main() {
	flag.Parse()
	bytes, _ := ioutil.ReadFile("input.txt")

	// After providing 1 to the only input instruction and passing all the tests,
	// what diagnostic code does the program produce?
	fmt.Println("Part 1: " + PartOne(string(bytes)))
	// And with input 5 and new opcodes supporting parameter modes?
	fmt.Println("Part 2: " + PartTwo(string(bytes)))
}

// PartOne passes 1 to input instruction, runs tests, and returns the diagnostic code produced by the program
func PartOne(input string) string {
	var inputProgram []int
	// Insert values to slice
	for _, s := range strings.Split(input, ",") {
		i, _ := strconv.Atoi(s)
		inputProgram = append(inputProgram, i)
	}

	// Run program
	runIntcode(inputProgram, inputOne)

	return "Output above should be all zeroes and a diagnostic code last"
}

// PartTwo passes 5 and returns a different diagnostic code
func PartTwo(input string) string {
	var inputProgram []int
	// Insert values to slice
	for _, s := range strings.Split(input, ",") {
		i, _ := strconv.Atoi(s)
		inputProgram = append(inputProgram, i)
	}
	// Run program
	runIntcode(inputProgram, inputTwo)

	return "Output above should be a single diagnostic code"
}

// runIntcode runs the program provided in an initial state and returns the final state.
// Adding modes: mode 0 interprets parameter as a position; mode 1 (immediate) interprets parameter as a value
func runIntcode(program []int, input int) {
	immediateMode := 1

	address := 0
	// If opcode == 99, halt (& pls don't catch fire O_o)
	for program[address] != 99 {
		// Opcode and modes for params can now be combined in the single int!
		var opcode, modeFirst, modeSecond, modeThird int
		opcodeAndModes := program[address]
		opcodeStr := strconv.Itoa(opcodeAndModes)
		if len(opcodeStr)-2 >= 0 {
			opcode, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-2:]))
		} else {
			opcode = opcodeAndModes
		}
		if len(opcodeStr)-3 >= 0 {
			modeFirst, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-3]))
		}
		if len(opcodeStr)-4 >= 0 {
			modeSecond, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-4]))
		}
		if len(opcodeStr)-5 >= 0 {
			modeThird, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-5]))
		}

		if opcode < 1 || opcode > 8 {
			// Encountering an unknown opcode means something went wrong
			fmt.Println("Something went wrong: unknown opcode!")
			fmt.Println(opcode)
			return
		}
		switch opcode {
		// If opcode == 1, read & sum values from the next 2 positions & store at 3rd value position, then step 4
		case 1:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if modeThird == immediateMode {
				storeParam = first + second
			} else {
				program[storeParam] = first + second
			}
			address += 4
		// If opcode == 2, read & multiply values from the next 2 positions & store at 3rd position, then step 4
		case 2:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if modeThird == immediateMode {
				storeParam = first * second
			} else {
				program[storeParam] = first * second
			}
			address += 4
		// If opcode == 3, take an input value and save to address at single parameter
		case 3:
			param := program[address+1]
			program[param] = input
			address += 2
		// If opcode == 4, output the value saved at the single parameter
		case 4:
			param := program[address+1]
			if modeFirst == immediateMode {
				fmt.Println(param)
			} else {
				fmt.Println(program[param])
			}
			address += 2
			// If opcode == 5, jump-if-true: *if* the first parameter is non-zero,
			// it sets the instruction pointer to the value from the second parameter
			// If the instruction modifies the instruction pointer, it is used next, not automatically increased
		case 5:
			firstParam := program[address+1]
			secondParam := program[address+2]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first != 0 {
				address = second
			} else {
				address += 3
			}
		// If opcode == 6, jump-if-false: *if* the first parameter is zero,
		// it sets the instruction pointer to the value from the second parameter
		// If the instruction modifies the instruction pointer, it is used next, not automatically increased
		case 6:
			firstParam := program[address+1]
			secondParam := program[address+2]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first == 0 {
				address = second
			} else {
				address += 3
			}
		// If opcode == 7, less than: *if* the first parameter is less than the second parameter,
		// it stores 1 in the position given by the third parameter
		case 7:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second, val int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first < second {
				val = 1
			}
			if modeThird == immediateMode {
				storeParam = val
			} else {
				program[storeParam] = val
			}
			address += 4
		// If opcode == 8, equals: *if* the first parameter is equal to the second parameter,
		// it stores 1 in the position given by the third parameter
		case 8:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second, val int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first == second {
				val = 1
			}
			if modeThird == immediateMode {
				storeParam = val
			} else {
				program[storeParam] = val
			}
			address += 4
		// Encountering an unknown opcode means something went wrong
		default:
			fmt.Println("This program is no good.")
			break
		}
	}
	return
}
