package main

import (
	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
)

// Day N: https://adventofcode.com/2019/day/N
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	fmt.Println("Part 1: " + PartOne(string(bytes)))
	fmt.Println("Part 2: " + PartTwo(string(bytes)))
}

// PartOne ...
func PartOne(input string) string {
	for _, s := range strings.Split(input, "\n") {
		_, _ = strconv.Atoi(s)
	}
	return ""
}

// PartTwo ...
func PartTwo(input string) string {
	for _, s := range strings.Split(input, "\n") {
		_, _ = strconv.Atoi(s)
	}
	return ""
}
