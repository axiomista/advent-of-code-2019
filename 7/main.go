package main

import (
	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
)

// Day 7: https://adventofcode.com/2019/day/7
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	// Find the largest output signal that can be sent to the thrusters, 
	// by trying every possible combination of phase settings on the amplifiers.
	fmt.Println("Part 1: " + PartOne(string(bytes)))
	fmt.Println("Part 2: " + PartTwo(string(bytes)))
}

// Input phaseSetting = N in [0,4]; each is used once by amps A-E
//     O-------O  O-------O  O-------O  O-------O  O-------O
// 0 ->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-> (to thrusters)
//     O-------O  O-------O  O-------O  O-------O  O-------O
// 

// PartOne try all combinations of phase settings and choose the largest signal.
// Each output should be the input for the next run. 
// The first amplifier's input is 0, and the last amplifier's output leads to your ship's thrusters.
func PartOne(input string) string {
	var maxSignal int
	var initState []int

	// Insert values to slice
	for _, s := range strings.Split(input, ",") {
		i, _ := strconv.Atoi(s)
		initState = append(initState, i)
	}

	// Generate permutations of [0,1,2,3,4] to try as phase settings
	for phases := range generatePermutations([]int{0,1,2,3,4}) {
		// Make this copy for A, B, C, D, E
		ampA := make([]int, len(initState))
		ampB := make([]int, len(initState))
		ampC := make([]int, len(initState))
		ampD := make([]int, len(initState))
		ampE := make([]int, len(initState))
		copy(ampA, initState)
		copy(ampB, initState)
		copy(ampC, initState)
		copy(ampD, initState)
		copy(ampE, initState)

		outputA := runIntcodeReturnOutput(ampA, phases[0], 0)
		outputB := runIntcodeReturnOutput(ampB, phases[1], outputA)
		outputC := runIntcodeReturnOutput(ampC, phases[2], outputB)
		outputD := runIntcodeReturnOutput(ampD, phases[3], outputC)
		signal := runIntcodeReturnOutput(ampE, phases[4], outputD)

		if signal > maxSignal {
			maxSignal = signal
		}
  }

	return strconv.Itoa(maxSignal)
}

// PartTwo ...
func PartTwo(input string) string {
	for _, s := range strings.Split(input, "\n") {
		_, _ = strconv.Atoi(s)
	}
	return ""
}
//  // Input 1: use phase setting value for this amplifier [0-4]
//  // Input 2: use input 0 for A, prev output for subsequent inputs ( -> B -> ...)
// func amplifier(program []int, input int, phaseSetting int) int {

// }

func runIntcodeReturnOutput(program []int, phaseSetting int, input int) int {
	var output int

	phaseSet := false
	immediateMode := 1
	address := 0
	// If opcode == 99, halt (& pls don't catch fire O_o)
	for program[address] != 99 {
		// Opcode and modes for params can now be combined in the single int!
		var opcode, modeFirst, modeSecond, modeThird int
		opcodeAndModes := program[address]
		opcodeStr := strconv.Itoa(opcodeAndModes)
		if len(opcodeStr)-2 >= 0 {
			opcode, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-2:]))
		} else {
			opcode = opcodeAndModes
		}
		if len(opcodeStr)-3 >= 0 {
			modeFirst, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-3]))
		}
		if len(opcodeStr)-4 >= 0 {
			modeSecond, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-4]))
		}
		if len(opcodeStr)-5 >= 0 {
			modeThird, _ = strconv.Atoi(string(opcodeStr[len(opcodeStr)-5]))
		}

		if opcode < 1 || opcode > 8 {
			// Encountering an unknown opcode means something went wrong
			fmt.Println("Something went wrong: unknown opcode!")
			fmt.Println(opcode)
			return -1
		}
		switch opcode {
		// If opcode == 1, read & sum values from the next 2 positions & store at 3rd value position, then step 4
		case 1:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if modeThird == immediateMode {
				storeParam = first + second
			} else {
				program[storeParam] = first + second
			}
			address += 4
		// If opcode == 2, read & multiply values from the next 2 positions & store at 3rd position, then step 4
		case 2:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if modeThird == immediateMode {
				storeParam = first * second
			} else {
				program[storeParam] = first * second
			}
			address += 4
		// If opcode == 3, take an input value (prev output or phase setting) and save to address at single parameter
		case 3:
			param := program[address+1]
			if phaseSet == false {
				program[param] = phaseSetting
				phaseSet = true
				address += 2
			} else {
				program[param] = input
				address += 2
			}
		// If opcode == 4, output the value saved at the single parameter
		case 4:
			param := program[address+1]
			output = program[param]
			address += 2
			// If opcode == 5, jump-if-true: *if* the first parameter is non-zero,
			// it sets the instruction pointer to the value from the second parameter
			// If the instruction modifies the instruction pointer, it is used next, not automatically increased
		case 5:
			firstParam := program[address+1]
			secondParam := program[address+2]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first != 0 {
				address = second
			} else {
				address += 3
			}
		// If opcode == 6, jump-if-false: *if* the first parameter is zero,
		// it sets the instruction pointer to the value from the second parameter
		// If the instruction modifies the instruction pointer, it is used next, not automatically increased
		case 6:
			firstParam := program[address+1]
			secondParam := program[address+2]
			var first, second int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first == 0 {
				address = second
			} else {
				address += 3
			}
		// If opcode == 7, less than: *if* the first parameter is less than the second parameter,
		// it stores 1 in the position given by the third parameter
		case 7:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second, val int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first < second {
				val = 1
			}
			if modeThird == immediateMode {
				storeParam = val
			} else {
				program[storeParam] = val
			}
			address += 4
		// If opcode == 8, equals: *if* the first parameter is equal to the second parameter,
		// it stores 1 in the position given by the third parameter
		case 8:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			var first, second, val int
			if modeFirst == immediateMode {
				first = firstParam
			} else {
				first = program[firstParam]
			}
			if modeSecond == immediateMode {
				second = secondParam
			} else {
				second = program[secondParam]
			}
			if first == second {
				val = 1
			}
			if modeThird == immediateMode {
				storeParam = val
			} else {
				program[storeParam] = val
			}
			address += 4
		// Encountering an unknown opcode means something went wrong
		default:
			fmt.Println("This program is no good.")
			break
		}
	}
	return output
}

func generatePermutations(data []int) <-chan []int {  
	c := make(chan []int)
	go func(c chan []int) {
			defer close(c) 
			permute(c, data)
	}(c)
	return c 
}

func permute(c chan []int, inputs []int){
	output := make([]int, len(inputs))
	copy(output, inputs)
	c <- output

	size := len(inputs)
	p := make([]int, size + 1)
	for i := 0; i < size + 1; i++ {
			p[i] = i;
	}
	for i := 1; i < size;{
			p[i]--
			j := 0
			if i % 2 == 1 {
					j = p[i]
			}
			tmp := inputs[j]
			inputs[j] = inputs[i]
			inputs[i] = tmp
			output := make([]int, len(inputs))
			copy(output, inputs)
			c <- output
			for i = 1; p[i] == 0; i++{
					p[i] = i
			}
	}
}