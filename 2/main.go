package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

// Day 2: https://adventofcode.com/2019/day/2
func main() {
	bytes, _ := ioutil.ReadFile("input.txt")

	// What value is left at position 0 after the program halts?
	fmt.Println("Part 1: " + PartOne(string(bytes)))
	// Find the input noun and verb that cause the program to produce the output 19690720. 
	// What is 100 * noun + verb?
	fmt.Println("Part 2: " + PartTwo(string(bytes)))
}

// PartOne takes the input (comma-separated ints), appends each value to an []int, and runs it as Intcode program;
// then we return the value at position 0 as the solution.
func PartOne(input string) string {
	var initial []int

	// Insert values to slice
	for _, s := range strings.Split(input, ",") {
		i, _ := strconv.Atoi(s)
		initial = append(initial, i)
	}
	// Update values to manually revert state
	initial = gravityAssistRestore(initial)
	// Run program
	final := runIntcode(initial)
	// Return final[0] as string value
	return strconv.Itoa(final[0])
}

// PartTwo takes the same Intcode program input and finds the pair of inputs that produce 19690720.
// Like the gravityAssistRestore function, program[1] = noun and program[2] = verb
func PartTwo(input string) string {
	var initialState []int

	// Insert values to slice
	for _, s := range strings.Split(input, ",") {
		i, _ := strconv.Atoi(s)
		initialState = append(initialState, i)
	}

	// Find initial[1] and initial[2] so that final[0] = 19690720
	for verb := 0; verb < 99; verb++ {
		for noun := 0; noun < 99; noun++ {
			initial := make([]int, len(initialState))
			copy(initial, initialState)
			initial[1] = noun
			initial[2] = verb
			// Run program
			final := runIntcode(initial)
			if final[0] == 19690720 {
				// Return 100 * noun + verb as string
				return strconv.Itoa(100 * noun + verb)
			}
		}
	}
	return ""
}

// runIntcode runs the program provided in an initial state and returns the final state.
func runIntcode(program []int) []int {
	address := 0
	// If opcode == 99, halt (& pls don't catch fire O_o)
	for program[address] != 99 {
		opcode := program[address]
		if (opcode != 1) && (opcode != 2) {
			// Encountering an unknown opcode means something went wrong
			return program
		}
		switch opcode {
		// If opcode == 1, read & sum values from the next 2 positions & store at 3rd value position, then step 4
		case 1:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			program[storeParam] = program[firstParam] + program[secondParam]
		// If opcode == 2, read & multiply values from the next 2 positions & store at 3rd position, then step 4
		case 2:
			firstParam := program[address+1]
			secondParam := program[address+2]
			storeParam := program[address+3]
			program[storeParam] = program[firstParam] * program[secondParam]
		// Encountering an unknown opcode means something went wrong
		default:
			fmt.Println("This program is no good.")
			break
		}
		address +=4
	}
	return program
}

// gravityAssistRestore reverts the gravity assist program to the state it had just before the last computer caught fire.
// Before running the program, replace position 1 with the value 12 and replace position 2 with the value 2.
func gravityAssistRestore(program []int) []int {
	program[1] = 12
	program[2] = 2
	return program
}
