package main

import (
	"fmt"
	"io/ioutil"

	"strconv"
	"strings"
)

// Day 6: https://adventofcode.com/2019/day/6
// MapPlanets is a hashmap of the Planets (name -> reference)
var MapPlanets map[string]*Planet

// Planet is the single object type from which the orbit map is composed
type Planet struct {
	name   string
	orbits *Planet
}

func main() {
	bytes, _ := ioutil.ReadFile("input.txt")
	MapPlanets = make(map[string]*Planet)

	// What is the total number of direct and indirect orbits in your map data?
	fmt.Println("Part 1: " + PartOne(string(bytes)))
	// What is the minimum number of orbital transfers required to move
	// from the object YOU are orbiting to the object SAN is orbiting?
	fmt.Println("Part 2: " + PartTwo())
}

// PartOne takes data like `XYZ)ABC \n ABC)CDE` meaning ABC orbits XYZ. and CDE orbits ABC,
// thus CDE indirectly orbits XYZ. It returns a sum of direct + indirect orbits.
func PartOne(input string) string {
	for _, s := range strings.Split(input, "\n") {
		addPlanetsFromData(s)
	}
	// Count & sum connections (distance) from each to the first planet where orbits == nil
	var totalOrbits int
	for _, p := range MapPlanets {
		totalOrbits += sumOrbits(p)
	}

	return strconv.Itoa(totalOrbits)
}

// PartTwo ...
func PartTwo() string {
	youPlanet := MapPlanets["YOU"]
	sanPlanet := MapPlanets["SAN"]
	// Find list of Planet names after YOU to base, after SAN to base
	path1 := orbitPath(youPlanet)
	path2 := orbitPath(sanPlanet)
	// Count distance between bodies
	var between string
	for _, p := range path1 {
		for _, q := range path2 {
			if q == p {
				between = q
			}
			if between != "" {
				break
			}
		}
		if between != "" {
			break
		}
	}
	// Count steps to shared planet between 1 and 2
	var stepsBetween int
	for _, step := range path1 {
		if step != between {
			stepsBetween++
		} else {
			break
		}
	}
	for _, step := range path2 {
		if step != between {
			stepsBetween++
		} else {
			break
		}
	}
	return strconv.Itoa(stepsBetween)
}

// sumOrbits counts all the orbits until a body orbits nothing
func sumOrbits(planet *Planet) int {
	var sum int
	for planet.orbits != nil {
		sum++
		planet = planet.orbits
	}
	return sum
}

// orbitPath returns a list of all the planet names in a body's direct and indirect orbit
func orbitPath(planet *Planet) []string {
	var path []string
	for planet.orbits != nil {
		planet = planet.orbits
		path = append(path, planet.name)
	}
	return path
}

// addPlanetsFromData takes two planets names e.g. VNN)EAG and maps the entities by name and orbit
func addPlanetsFromData(orbitData string) {
	var basePlanet *Planet
	var orbitingPlanet *Planet
	// Construct planets and add planet data to the orbit map
	p := strings.Split(orbitData, ")")
	basePlanetName := p[0]
	orbitingPlanetName := p[1]

	// Check for base planet in our map
	basePlanet, ok1 := MapPlanets[basePlanetName]
	if !ok1 {
		// Make base planet if it doesn't exist
		bp := Planet{
			name:   basePlanetName,
			orbits: nil,
		}
		basePlanet = &bp
		MapPlanets[basePlanetName] = &bp
	}

	// Check for orbiting planet in our map
	orbitingPlanet, ok2 := MapPlanets[orbitingPlanetName]
	if ok2 {
		// Add orbit pointer
		orbitingPlanet.orbits = basePlanet
	} else {
		// Make orbiting planet if it doesn't exist
		op := Planet{
			name:   orbitingPlanetName,
			orbits: basePlanet,
		}
		orbitingPlanet = &op
		MapPlanets[orbitingPlanetName] = &op
	}
}
